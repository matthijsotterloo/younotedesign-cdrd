<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Cor de Rover Design - Yacht design</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <meta name="author" content="Gerko Massier" />
      <meta name="description" content="We designing exclusive luxury Yachts" />
      <meta name="keywords" content="superyachts,superyacht,yacht,design,yachtdesign,boat,boats,for,sale,monaco,sailingyacht,sailyacht,sail,luxury,cor,de,rover,corderoverdesign," />
      <link rel="stylesheet" href="css/bootstrap.min.css"/>
      <link rel="stylesheet" href="css/style.css"/>
      <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
      <?php require('head.php');?>
      <?php require('fullpage-yacht.php');?>
   </head>
   <body id="yacht-design-page">
      <?php include ('menu-normal.php');?>
      <div id="fullpage">
         <!-- Slide 1. -->
         <div class="section project-slide">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-12">
                     <div class="padding-top-header">
                        <div class="slide-caption">
                           <h2 class="text-uppercase subtitle-head">projecten</h2>
                           <h3 class="subtitle-sub-head">hier komen de thumbnails van alle projecten &lt;24M </h3>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Slide 2. -->
         <div class="section project-slide">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-12">
                     <div class="padding-top-header">
                        <div class="slide-caption">
                           <h2 class="text-uppercase subtitle-head">projecten</h2>
                           <h3 class="subtitle-sub-head">hier komen de thumbnails van alle projecten &lt;50M</h3>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Slide 3. -->
         <div class="section project-slide">
            <div class="slide project-1-1">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">render 1</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project (en info?)</h3>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Slide 4. -->
            <div class="slide project-1-2">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">render 2</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project</h3>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Slide 5. -->
            <div class="slide project-1-3">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">render 3</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project</h3>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Slide 6. -->
            <div class="slide project-1-4">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">info over het project</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project</h3>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Slide 7. -->
         <div class="section project-slide">
            <div class="slide project-1-1">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">render 1</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project (en info?)</h3>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Slide 8. -->
            <div class="slide project-1-2">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">render 2</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project</h3>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Slide 9. -->
            <div class="slide project-1-3">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">render 3</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project</h3>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Slide 10. -->
            <div class="slide project-1-4">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">info over het project</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project</h3>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Slide 11. -->
         <div class="section project-slide">
            <div class="slide project-1-1">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">render 1</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project (en info?)</h3>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Slide 11. -->
            <div class="slide project-1-2">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">render 2</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project</h3>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Slide 12. -->
            <div class="slide project-1-3">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">render 3</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project</h3>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Slide 13. -->
            <div class="slide project-1-4">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">info over het project</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project</h3>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Slide 14. -->
         <div class="section project-slide">
            <div class="slide project-1-1">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">render 1</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project (en info?)</h3>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Slide 15. -->
            <div class="slide project-1-2">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">render 2</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project</h3>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Slide 16. -->
            <div class="slide project-1-3">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">render 3</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project</h3>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Slide 17. -->
            <div class="slide project-1-4">
               <div class="col-md-12">
                  <div class="padding-top-header">
                     <div class="slide-caption">
                        <h2 class="text-uppercase subtitle-head">info over het project</h2>
                        <h3 class="subtitle-sub-head">hier komt een fullscreen beeld van het project</h3>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php include ('footer.php');?>
   </body>
</html>
