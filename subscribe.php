<div id="subscribe-slide">
   <div class="container">
      <div class="row">
         <div class="col-md-6 left-col-subscirbe">
            <h5 class="text-right">Stay up-to-date</h5>
            <p class="text-right">Sign up to our newsletter for the latest news and<br>things about Cor de Rover design.</p>
         </div>
         <div class="col-md-6 right-col-subsribe">
            <form id="subscribe-form" method="POST" action="?p=subscribe">
               <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter your email address">
               <button id="subscribe-button" type="submit" value="Submit" name="Submit">Send</button>
            </form>
         </div>
      </div>
   </div>
</div>
