<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Cor de Rover Design - Home</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <meta name="author" content="Gerko Massier" />
      <meta name="description" content="We designing exclusive luxury Yachts" />
      <meta name="keywords" content="superyachts,superyacht,yacht,design,yachtdesign,boat,boats,for,sale,monaco,sailingyacht,sailyacht,sail,luxury,cor,de,rover,corderoverdesign," />
      <link rel="stylesheet" href="css/bootstrap.min.css"/>
      <link rel="stylesheet" href="css/style.css"/>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"/>
      <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
      <?php require('head.php');?>
      <?php require('fullpage-home.php');?>
   </head>
   <body id="home-page">
      <?php include ('includes/menu-home.php');?>
      <div id="fullpage">
         <!-- Slide 1. -->
         <div class="section home-slide">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-12">
                     <div class="padding-top-header">
                        <div class="slide-caption">
                           <h2 class="text-uppercase subtitle-head"><span>it starts with a</span><br><span>few lines</span></h2>
                           <h3 class="subtitle-sub-head"><span>Scroll down or use the arrows to explore our website</span></h3>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Slide 2. -->
         <div class="section yachtdesign-slide">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-12 home-title">
                     <div class="padding-top-header">
                        <div class="slide-caption">
                           <h2 class="text-uppercase subtitle-head"><span>Our yachtdesign</span><br><span>projects</span></h2>
                           <h3 class="subtitle-sub-head"><span>click the button below to explore our projects</span></h3>
                           <a href="yacht-design.php" class="btn btn-default btn-lg">See projects</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Slide 3. -->
         <div class="section interior-slide">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-12 home-title">
                     <div class="padding-top-header">
                        <div class="slide-caption">
                           <h2 class="text-uppercase subtitle-head"><span>Our interior design</span><br><span>projects</span></h2>
                           <h3 class="subtitle-sub-head"><span>click the button below to explore our interior designs</span></h3>
                           <a href="interior-design.php" class="btn btn-default btn-lg">See more</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Slide 4. -->
         <div class="section naval-architect-slide">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-12 home-title">
                     <div class="padding-top-header">
                        <div class="slide-caption">
                           <h2 class="text-uppercase subtitle-head"><span>about our</span><br><span>naval architecture</span></h2>
                           <h3 class="subtitle-sub-head"><span>click the button below to explore our naval architecture</span></h3>
                           <a href="naval-architecture.php" class="btn btn-default btn-lg">Read more</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Slide 5. -->
         <div class="section news-slide">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-12 home-title">
                     <div class="padding-top-header">
                        <div class="slide-caption">
                           <h2 class="text-uppercase subtitle-head"><span>stay up-to-date</span><br><span>read our news here</span></h2>
                           <h3 class="subtitle-sub-head"><span>click the button below to read our news</span></h3>
                           <a href="news.php" class="btn btn-default btn-lg">Read more</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Slide 6. -->
         <div class="section contact-slide">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-12 home-title">
                     <div class="padding-top-header">
                        <div class="slide-caption">
                           <h2 class="text-uppercase subtitle-head"><span>Find here your</span><br><span>contact information</span></h2>
                           <h3 class="subtitle-sub-head"><span>click the button below to contact us</span></h3>
                           <a href="contact.php" class="btn btn-default btn-lg">Contact us</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php include ('footer.php');?>
   </body>
</html>
