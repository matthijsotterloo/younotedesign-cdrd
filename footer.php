<div id="footer">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-4 col-xs-12">
            <ul class="social-menu">
               <li><a href="#"><img src="images/social/facebook.png"/ height="15"></a></li>
               <li><a href="#"><img src="images/social/linkedin.png"/ height="15"></a></li>
               <li><a href="#"><img src="images/social/instagram.png"/ height="15"></a></li>
               <li><a href="#"><img src="images/social/youtube.png"/ height="15"></a></li>
               <li><a href="#"><img src="images/social/vimeo.png"/ height="15"></a></li>
               <li><a href="#"><img src="images/nl-flag.png" height="15"/></a></li>
               <li class="last-social"><a href="#"><img src="images/en-flag.png" height="15"/></a></li>
            </ul>
         </div>
         <div class="col-md-4 col-xs-12 text-center">
            <?php require('home-arrows.php');?>
         </div>
         <div class="col-md-4 col-xs-12">
            <p class="copy-text">&copy; Cor d Rover design | webdesign by: &nbsp;
               <a href="http://www.madebygerko.com" target="_blank">
				   <img src="images/logo-madebygerko-wit.png" width="150"/>
			   </a>
            </p>
         </div>
      </div>
   </div>
</div>
