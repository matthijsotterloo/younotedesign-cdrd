<!-- Models slider -->
<div class="container">
   <div class="row">
      <div id="24-slider" class="carousel slide hidden-xs models" style="display: none">
         <div class="carousel-inner">
            <div class="item active">
               <div class="row">
                  <div class="col-sm-3">
                     <div class="col-item">
                        <div class="photo">
                           <img src="http://placehold.it/350x260" class="img-responsive" alt="a" />
                        </div>
                     </div>
                     <div class="description">
                        <p>Boot van 24 meter</p>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="col-item">
                        <div class="photo">
                           <img src="http://placehold.it/350x260" class="img-responsive" alt="a" />
                        </div>
                     </div>
                     <div class="description">
                        <p>Boot van 24 meter</p>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="col-item">
                        <div class="photo">
                           <img src="http://placehold.it/350x260" class="img-responsive" alt="a" />
                        </div>
                     </div>
                     <div class="description">
                        <p>Boot van 24 meter</p>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="col-item">
                        <div class="photo">
                           <img src="http://placehold.it/350x260" class="img-responsive" alt="a" />
                        </div>
                     </div>
                     <div class="description">
                        <p>Boot van 24 meter</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Left and right controls -->
      <div id="slider-control" style="display: none">
         <a class="left carousel-control" href="#models-carousel" role="button" data-slide="prev">
         <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
         </a>
         <a class="right carousel-control" href="#models-carousel" role="button" data-slide="next">
         <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
         </a>
      </div>
   </div>
</div>
