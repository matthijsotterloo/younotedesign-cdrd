<nav class="navbar navbar-default navbar-fixed-top">
   <div class="navbar-inner">
      <div class="container-fluid">
         <div class="row">
            <div class="col-xs-2 border">
               <button type="button" id="sizes" class="btn btn-defult navbar-brand">Models</button>
            </div>
            <div class="col-xs-8 border">
               <a class="navbar-brand logo-center" href="index.php"><img src="images/logo-chrome.png" / width="250"></a>
            </div>
            <div class="col-xs-2 border">
               <button type="button" id="menu" class="btn btn-defult navbar-brand right">Menu</button>
            </div>
         </div>

         <!-- Include sizes and models slider -->
         <?php include('includes/sizes-slider.php'); ?>
         <?php include('includes/models-slider.php'); ?>

         <!-- Include Yacht Sizes -->
         <?php include('includes/sizes/all-slider.php'); ?>
         <?php include('includes/sizes/24-slider.php'); ?>
         <?php include('includes/sizes/50-slider.php'); ?>

         <!-- Include menu links -->
         <?php include('includes/menu-links.php'); ?>
   </div>
   </div>
</nav>
