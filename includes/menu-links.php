<!-- Menu links -->
<div class="container">
   <div class="row">
      <div id="menu-carousel" class="carousel slide hidden-xs menu" style="display: none">
         <div class="carousel-inner">
            <div class="item active">
               <div class="row">
                  <div class="col-sm-12 menu-center">
                     <a href="#home">
                        <button type="button" class="btn btn-defult" aria-label="Left Align">
                           <p class="text-uppercase" data-menuanchor="home">Home</p>
                        </button>
                     </a>
                     <a href="#yachtdesign">
                        <button type="button" class="btn btn-defult" aria-label="Left Align">
                           <p class="text-uppercase" data-menuanchor="yachtdesign">Yachtdesign</p>
                        </button>
                     </a>
                     <a href="#interior">
                        <button type="button" class="btn btn-defult" aria-label="Left Align">
                           <p class="text-uppercase" data-menuanchor="interior">Interior</p>
                        </button>
                     </a>
                     <a href="#naval-architect">
                        <button type="button" class="btn btn-defult" aria-label="Left Align">
                           <p class="text-uppercase" data-menuanchor="naval-architect">Naval Architect</p>
                        </button>
                     </a>
                     <a href="#news">
                        <button type="button" class="btn btn-defult" aria-label="Left Align">
                           <p class="text-uppercase" data-menuanchor="news">News</p>
                        </button>
                     </a>
                     <a href="#contact">
                        <button type="button" class="btn btn-defult" aria-label="Left Align">
                           <p class="text-uppercase" data-menuanchor="contact">Contact</p>
                        </button>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
