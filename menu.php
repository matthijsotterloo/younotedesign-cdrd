<nav class="navbar navbar-default navbar-fixed-top">
   <div class="navbar-inner">
      <div class="container">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="images/logo-Cor-D-Rover-black.png" / width="250"></a>
         </div>
         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
               <li><a href="#page1">HOME</a></li>
               <li class="menu-bullet"></li>
               <li><a href="#page2">YACHTDESIGN</a></li>
               <li class="menu-bullet"></li>
               <li><a href="#">INTERIOR</a></li>
               <li class="menu-bullet"></li>
               <li><a href="#">NAVAL ARCHITECT</a></li>
               <li class="menu-bullet"></li>
               <li><a href="#">NEWS</a></li>
               <li class="menu-bullet"></li>
               <li><a href="#">CONTACT</a></li>
            </ul>
         </div>
      </div>
   </div>
</nav>
