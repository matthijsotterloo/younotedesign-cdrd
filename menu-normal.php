<nav class="navbar navbar-default navbar-fixed-top">
   <div class="navbar-inner">
      <div class="container-fluid">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="images/logo-Cor-D-Rover.png" / width="250"></a>
         </div>
         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
               <li><a href="index.php">HOME</a></li>
               <li class="menu-bullet"></li>
               <li><a href="yacht-design.php">YACHTDESIGN</a></li>
               <li class="menu-bullet"></li>
               <li><a href="interior-design.php">INTERIOR</a></li>
               <li class="menu-bullet"></li>
               <li><a href="naval-architecture.php">NAVAL ARCHITECT</a></li>
               <li class="menu-bullet"></li>
               <li><a href="news.php">NEWS</a></li>
               <li class="menu-bullet"></li>
               <li><a href="contact.php">CONTACT</a></li>
            </ul>
         </div>
      </div>
   </div>
</nav>
