<div id="social-slide">
   <div class="container">
      <div class="row">
         <div class="col-md-6">
            <h4 class="social-title text-uppercase">Get social with us</h4>
         </div>
         <div class="col-md-6">
            <ul class="social-menu">
               <li><a href="#"><img src="images/social/twitter.png"/ width="35"></a></li>
               <li><a href="#"><img src="images/social/facebook.png"/ width="35"></a></li>
               <li><a href="#"><img src="images/social/skype.png"/ width="35"></a></li>
               <li><a href="#"><img src="images/social/linkedin.png"/ width="35"></a></li>
               <li><a href="#"><img src="images/social/instagram.png"/ width="35"></a></li>
               <li><a href="#"><img src="images/social/youtube.png"/ width="35"></a></li>
               <li class="last-social"><a href="#"><img src="images/social/vimeo.png"/ width="35"></a></li>
            </ul>
         </div>
      </div>
   </div>
</div>
