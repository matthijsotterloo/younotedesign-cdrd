$(document).ready(function() {
    $('#fullpage').fullpage({
        anchors: ['home', 'yachtdesign', 'interior', 'naval-architect', 'news', 'contact'],
        navigation: true,
        sectionsColor: ['#000', '#000', '#000', '#000', '#000', '#000'],
        scrollingSpeed: 1800,
        navigationPosition: 'right',
        navigationTooltips: ['Overview <24M', 'Overview <50M', 'Yacht 1', 'Yacht 4', 'Yacht 5', 'Yacht6'],
        showActiveTooltip: true,
        responsiveWidth: 300,
        verticalCentered: false,
        css3: true,
        controlArrows: false,
        keyboardScrolling: true,
        recordHistory: false,
        slidesNavigation: true,
        slidesNavPosition: 'bottom',
    });

    //Sizes button action
    $('#sizes').click(function(){
        //Hide other divs
        $('#menu-carousel').hide('fast','linear');
        $('#all-slider').hide('fast','linear');
        $('#24-slider').hide('fast','linear');
        $('#50-slider').hide('fast','linear');

        //Check if open/closed
        if($('#sizes-carousel').css('display') == 'none'){
            $('#sizes-carousel').show('fast', 'linear');
        } else {
            $('#sizes-carousel').hide('fast', 'linear');
        }
    });

    //Menu button action
    $('#menu').click(function(){
        //Hide other divs
        $('#sizes-carousel').hide('fast','linear');
        $('#all-slider').hide('fast','linear');
        $('#24-slider').hide('fast','linear');
        $('#50-slider').hide('fast','linear');
        //Check if open/closed
        if($('#menu-carousel').css('display') == 'none'){
            $('#menu-carousel').show('fast', 'linear');
        } else {
            $('#menu-carousel').hide('fast', 'linear');
        }
    });

    //All-sizes action
    $('#all-open').click(function(){
        //Hide other divs
        $('#sizes-carousel').hide('fast','linear');
        $('#menu-carousel').hide('fast','linear');
        $('#all-slider').hide('fast','linear');
        $('#24-slider').hide('fast','linear');
        $('#50-slider').hide('fast','linear');
        //Show new div
        $('#all-slider').show('fast','linear');
    });

    //24 meter button action
    $('#24-open').click(function(){
        //Hide other divs
        $('#sizes-carousel').hide('fast','linear');
        $('#menu-carousel').hide('fast','linear');
        $('#all-slider').hide('fast','linear');
        $('#24-slider').hide('fast','linear');
        $('#50-slider').hide('fast','linear');
        //Show new div
        $('#24-slider').show('fast','linear');
    });

    //50 meter button action
    $('#50-open').click(function(){
        //Hide other divs
        $('#sizes-carousel').hide('fast','linear');
        $('#menu-carousel').hide('fast','linear');
        $('#all-slider').hide('fast','linear');
        $('#24-slider').hide('fast','linear');
        $('#50-slider').hide('fast','linear');
        //Show new div
        $('#50-slider').show('fast','linear');
    });
});

$(document).on('click', '#moveUp', function(){
    $.fn.fullpage.moveSectionUp();
});

$(document).on('click', '#moveDown', function(){
    $.fn.fullpage.moveSectionDown();
});

$(document).on('click', '#moveLeft', function(){
    $.fn.fullpage.moveSlideLeft();
});

$(document).on('click', '#moveLeft', function(){
    $.fn.fullpage.setMouseWheelScrolling(false);
});

$('.navbar').on('show.bs.collapse', function () {
        var actives = $(this).find('.collapse.in'), hasData;

        if (actives && actives.length) {
            hasData = actives.data('collapse')

        if (hasData && hasData.transitioning) return
            actives.collapse('hide')
            hasData || actives.data('collapse', null)
        }
});

jQuery(document).ready(function(event){
	var projectsContainer = $('.cd-projects-container'),
		navigation = $('.cd-primary-nav'),
		triggerNav = $('.cd-nav-trigger'),
		logo = $('.cd-logo');

	triggerNav.on('click', function(){
		if( triggerNav.hasClass('project-open') ) {
			//close project
			projectsContainer.removeClass('project-open').find('.selected').removeClass('selected').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				$(this).children('.cd-project-info').scrollTop(0).removeClass('has-boxshadow');

			});
			triggerNav.add(logo).removeClass('project-open');
		} else {
			//trigger navigation visibility
			triggerNav.add(projectsContainer).add(navigation).toggleClass('nav-open');
		}
	});

	projectsContainer.on('click', '.single-project', function(){
		var selectedProject = $(this);
		if( projectsContainer.hasClass('nav-open') ) {
			//close navigation
			triggerNav.add(projectsContainer).add(navigation).removeClass('nav-open');
		} else {
			//open project
			selectedProject.addClass('selected');
			projectsContainer.add(triggerNav).add(logo).addClass('project-open');
		}
	});

	projectsContainer.on('click', '.cd-scroll', function(){
		//scroll down when clicking on the .cd-scroll arrow
		var visibleProjectContent =  projectsContainer.find('.selected').children('.cd-project-info'),
			windowHeight = $(window).height();

		visibleProjectContent.animate({'scrollTop': windowHeight}, 300);
	});

	//add/remove the .has-boxshadow to the project content while scrolling
	var scrolling = false;
	projectsContainer.find('.cd-project-info').on('scroll', function(){
		if( !scrolling ) {
		 	(!window.requestAnimationFrame) ? setTimeout(updateProjectContent, 300) : window.requestAnimationFrame(updateProjectContent);
		 	scrolling = true;
		}
	});

	function updateProjectContent() {
		var visibleProject = projectsContainer.find('.selected').children('.cd-project-info'),
			scrollTop = visibleProject.scrollTop();
		( scrollTop > 0 ) ? visibleProject.addClass('has-boxshadow') : visibleProject.removeClass('has-boxshadow');
		scrolling = false;
	}
});
